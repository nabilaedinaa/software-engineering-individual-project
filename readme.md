# Software Engineering - Individual Project
Name         : Nabila Edina 

NPM          : 1806173531

Project Name : Daily Planner

Links        :
 - Documentation : https://docs.google.com/document/d/1asMHMFYfZEckn1k7StMOH-WXlvjNd9CwYurzDUamM-s/edit
 - Works Log : https://docs.google.com/spreadsheets/d/1fSKiKIu-KXmD-EClffMn62KEsC0w5PzD-sRKTzmw9I0/edit#gid=0
 
### Description
Daily Planner is a program for user to make an activity or event planner.
It show the date along with the activities or events that have been added in a list view. 
Daily Planner.Planner also can add and delete an event repeatedly and clearing a day’s schedule.

### How to Use
1. Run `git clone https://gitlab.com/nabilaedinaa/software-engineering-individual-project.git` in cmd or Terminal
2. Open the file in your IDE (IntelliJ, VSCode, etc)
3. Go to the Planner folder and open Planner.java
4. Run the main in Planner.java
5. Application is started.