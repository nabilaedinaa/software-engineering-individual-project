package Test;
import Planner.Planner;
import Reader.Reader;
import Writer.Writer;

import java.io.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util. *;

public class TestAddTask {
    private static Scanner input;
    public static void main(String[] args) throws IOException {
        Scanner input = new Scanner(System.in);
        String line = null;
        ArrayList<String> fileContents = new ArrayList<>();
        File file = new File("planner.txt");
        file.createNewFile();
        String command = "add";
        String date = "02 26 2020";
        String time = "13:00";
        String description = "Do homework";
        String[] dateEvent = date.split(" ");
        int month = Integer.parseInt(dateEvent[0]);
        int day = Integer.parseInt(dateEvent[1]);
        int year = Integer.parseInt(dateEvent[2]);

        try{
            System.out.println("Testing add task...");
            System.out.println();
            System.out.println("-----" + month + "/" + day + "/" + year + "-----");
            String event = time + " " + description;
            System.out.println(event);
            System.out.println("-------------------");
            System.out.println();
            System.out.println("Event added");

        } catch (Exception e){
            System.out.println("Error");
        }
        System.out.println();
        System.out.println("Testing completed");
    }
}
