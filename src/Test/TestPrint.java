package Test;
import Planner.Planner;
import Reader.Reader;
import Writer.Writer;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class TestPrint {
    public static void main(String[] args) throws IOException {
        File file = new File("planner.txt");
        String date = "02 26 2020";
        String time = "13:00";
        String description = "Do homework";
        String[] dateEvent = date.split(" ");
        int month = Integer.parseInt(dateEvent[0]);
        int day = Integer.parseInt(dateEvent[1]);
        int year = Integer.parseInt(dateEvent[2]);
        try{
            System.out.println("Testing print...");
            System.out.println();

            System.out.println("Printing Today's schedule...");
            System.out.println();
            Reader.printToday(file);
            System.out.println();
            System.out.println("Printing " + month + "/" + day + "/" + year +"'s schedule...");
            System.out.println();
            Reader.printDay(month, day, year, file);
            System.out.println();
            System.out.println("Printing week's schedule...");
            System.out.println();
            Reader.printWeek(file);
        }
        catch (Exception e){
            System.out.println("Error");
        }
        System.out.println();
        System.out.println("Testing completed");

    }
}
