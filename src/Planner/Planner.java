package Planner;
import Reader.Reader;
import Writer.Writer;

import java.util.Scanner;
import java.io.*;

public class Planner {
    public static void main(String args[]) {

        Scanner input = new Scanner(System.in);
        File file = new File("planner.txt");
        boolean running = true;
        String event = new String();
        int month;

        System.out.println("Welcome to Daily Planner.Planner!");
        System.out.println("We will help you plan your day!");
        System.out.println();

        //search in current directory for existing file, if not, set up or goodbye
        if (!file.exists()) {
            System.out.println("Since you're new to this app, you need to create planner.txt required for planning. Would you like to create one? yes/no?");

            String create = input.next();

            if (create.equals("yes")) {
                try {
                    file.createNewFile();
                    RandomAccessFile accessor = new RandomAccessFile (file, "rw");
                    accessor.seek(0);
                    accessor.writeBytes(System.getProperty("line.separator"));
                }
                catch (IOException e) {
                    e.printStackTrace();
                }
            }
            else {
                System.out.println("Alright, try again later.");
                System.exit(1);
            }

        }

        while (running) {
            System.out.println("Please enter a command. Type help for options.");
            //Wait for user input
            String command[] = input.nextLine().split(" ", -2);

            switch (command[0]) {
                case "help":
                    help();
                    break;
                case "today":
                    Reader.printToday(file);
                    break;
                case "week":
                    Reader.printWeek(file);
                    break;
                case "print":
                    month = monthFormatter(command[1]);
                    Reader.printDay(month, Integer.parseInt(command[2]), Integer.parseInt(command[3]), file);
                    break;
                case "add":
                    month = monthFormatter(command[1]);
                    Writer.addEvent(month, Integer.parseInt(command[2]), Integer.parseInt(command[3]), file);
                    break;
                case "remove":
                    month = monthFormatter(command[1]);
                    System.out.println("Please enter the event you'd like to remove :");
                    event = input.nextLine();
                    Writer.deleteEvent(month, Integer.parseInt(command[2]), Integer.parseInt(command[3]), event, file);
                    break;
                case "clear":
                    month = monthFormatter(command[1]);
                    Writer.clear(month, Integer.parseInt(command[2]), Integer.parseInt(command[3]), file);
                case "exit":
                    running = false;
                    break;
                default:
                    break;
            }
        }

    }
    public static void help() {
        System.out.println();
        System.out.println("'today': to print today's schedule.");
        System.out.println("'week': to print this week's schedule.");
        System.out.println("'print MM DD YYYY': to print a certain day's schedule.");
        System.out.println("'add MM DD YYYY': to add an event to the schedule.");
        System.out.println("'remove MM DD YYYY': to delete a singular event. You will be prompted next to give the event.");
        System.out.println("'clear MM DD YYYY': to clear a day's schedule.");
        System.out.println("'exit': to exit.");
        System.out.println();
    }

    public static int monthFormatter(String input) {
        if (input.charAt(0)=='0') {
            return Character.getNumericValue(input.charAt(1));
        }
        else {
            return Integer.parseInt(input);
        }
    }
}